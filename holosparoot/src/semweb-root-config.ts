import { registerApplication, start } from "single-spa";

/*
registerApplication({
  name: "@single-spa/welcome",
  app: () =>
    System.import(
      "https://unpkg.com/single-spa-welcome/dist/single-spa-welcome.js"
    ),
  activeWhen: ["/"],
});
*/

registerApplication({
  name: "@semwebsrv/holospakeycloakauth",
  app: () => System.import("@semwebsrv/holospakeycloakauth"),
  activeWhen: ["/"],
});

registerApplication({
  name: "@semwebsrv/holospaapp",
  app: () => System.import("@semwebsrv/holospaapp"),
  activeWhen: ["/"],
});

registerApplication({
  name: "@semwebsrv/holohelloworldmicroapp",
  app: () => System.import("@semwebsrv/holohelloworldmicroapp"),
  activeWhen: ["/helloworld"],
});

registerApplication({
  name: "@semwebsrv/holoexplorermicroapp",
  app: () => System.import("@semwebsrv/holoexplorermicroapp"),
  activeWhen: ["/explorer"],
});

// registerApplication({
//   name: "@semweb/navbar",
//   app: () => System.import("@semweb/navbar"),
//   activeWhen: ["/"]
// });

start({
  urlRerouteOnly: true,
});
